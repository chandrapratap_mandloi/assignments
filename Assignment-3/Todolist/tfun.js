var taskInput=document.getElementById("new-task");//Add a new task.
var addButton=document.getElementsByTagName("button")[0];//first button
var incompleteTaskHolder=document.getElementById("incomplete-tasks");
var completedTasksHolder=document.getElementById("completed-tasks");
var deletedTaskHolder=document.getElementById("deleted-tasks");

//New task list item
var createNewTaskElement=function(taskString){
var listItem=document.createElement("li");
var checkBox=document.createElement("input");
var label=document.createElement("label");
var deleteButton=document.createElement("button");
  label.innerText=taskString;
  checkBox.type="checkbox";
  deleteButton.innerText="Delete";
  deleteButton.className="delete";
  listItem.appendChild(checkBox);
  listItem.appendChild(label);
  listItem.appendChild(deleteButton);
  return listItem;
}
var a;
function load(){
  console.log("inside Load fun");

   a= window.localStorage.getItem('taskList');
  if(a == null || a === undefined || a.length==0)
  {
    window.localStorage.setItem('taskList', JSON.stringify([]));
    let ab= window.localStorage.getItem('taskList');
    //console.log('after set', ab);
  }
  var tasks = JSON.parse(window.localStorage.getItem('taskList'));
  // console.log('onload task list is: ', tasks);
  for (var i = 0; i < tasks.length; i++) 
  {
  	
  	var abc=tasks[i].task;
  	console.log(abc);
  	var listItem=createNewTaskElement(abc);
  	if(tasks[i].status=="incomplete")
  	{
  		incompleteTaskHolder.appendChild(listItem);
  		bindTaskEvents(listItem, taskCompleted);
  	}	
  	else
  	{	
  		completedTasksHolder.appendChild(listItem);
  		bindTaskEvents(listItem,taskIncomplete);
  	}
  } 
}
var
 addTask=function()
{
  if(taskInput.value.match(/^\s*\s*$/) ) 
  {
    alert("Enter Valid Title!");
    false;
  }
  else
  {
    var tasks = JSON.parse(window.localStorage.getItem('taskList'));
    // console.log(tasks);
    var pushd = tasks.push({task: taskInput.value, status: 'incomplete'});
    console.log(tasks);
    window.localStorage.setItem('taskList',JSON.stringify(tasks));
    let a = JSON.parse(window.localStorage.getItem('taskList'));
    var listItem=createNewTaskElement(taskInput.value);
    incompleteTaskHolder.appendChild(listItem);
    bindTaskEvents(listItem, taskCompleted);
    taskInput.value="";
  }  
}



var listItem=this.parentNode;


//Delete task.
var deleteTask=function(){
    console.log("Delete Task...");
    var listItem=this.parentNode;
    var ul=listItem.parentNode;
    // var label=listItem.innerText.split('\n')[0];
    var text=listItem.childNodes[1].textContent;
    ul.removeChild(listItem);
    var tasks= JSON.parse(window.localStorage.getItem('taskList'));
    console.log(tasks);

   	// var index = tasks.findIndex(x => x.task=="label");
   	// console.log("Index is:", index)
   	// tasks.splice(index, 1);
   	for (var i = 0; i < tasks.length; i++) 
   	{
   		console.log(tasks[i].task)
   		if(tasks[i].task===text)
   		{
   			tasks.splice(i,1)
   		}
   	}
  	window.localStorage.setItem('taskList', JSON.stringify(tasks));
  	let getTask = JSON.parse(window.localStorage.getItem('taskList'));
}
var taskCompleted=function(){
  var listItem=this.parentNode;
  var tasks = JSON.parse(window.localStorage.getItem('taskList'));
  console.log("tasks", tasks.task);
  var label=listItem.innerText.split('\n')[0];
  for(var i=0;i<tasks.length;i++)
  {
    if(tasks[i].task===label)
    {
      tasks[i].status="complete";
    }
  }
  console.log(tasks)
  window.localStorage.setItem('taskList', JSON.stringify(tasks));
  let getTask = JSON.parse(window.localStorage.getItem('taskList'));
  completedTasksHolder.appendChild(listItem);
  bindTaskEvents(listItem, taskIncomplete);
}
var taskIncomplete=function(){
    console.log("Incomplete Task...");
    var listItem=this.parentNode;
    var tasks = JSON.parse(window.localStorage.getItem('taskList'));
    var label=listItem.innerText.split('\n')[0];
  	for(var i=0;i<tasks.length;i++)
  	{
    	if(tasks[i].task===label)
    	{
      		tasks[i].status="incomplete";
    	}
  	}
  	console.log(tasks)
    
    //tasks.push({task: listItem.textContent, status: 'Incomplete'});
    window.localStorage.setItem('taskList', JSON.stringify(tasks));
    let getTask = JSON.parse(window.localStorage.getItem('taskList'));
    incompleteTaskHolder.appendChild(listItem);
    bindTaskEvents(listItem,taskCompleted);
}




var ajaxRequest=function(){
  console.log("AJAX Request");
}

//The glue to hold it all together.


//Set the click handler to the addTask function.
addButton.onclick=addTask;
//addButton.addEventListener("click",addTask);
//addButton.addEventListener("click",ajaxRequest);


var bindTaskEvents=function(taskListItem,checkBoxEventHandler){
  //console.log("bind list item events");
//select ListItems children
  var checkBox=taskListItem.querySelector("input[type=checkbox]");
  
  var deleteButton=taskListItem.querySelector("button.delete");


     
      //Bind deleteTask to delete button.
      deleteButton.onclick=deleteTask;
      //Bind taskCompleted to checkBoxEventHandler.
      checkBox.onchange=checkBoxEventHandler;
}

//cycle over incompleteTaskHolder ul list items
  //for each list item
  for (var i=0; i<incompleteTaskHolder.children.length;i++){

    //bind events to list items chldren(tasksCompleted)
    bindTaskEvents(incompleteTaskHolder.children[i],taskCompleted);
  }




//cycle over completedTasksHolder ul list items
  for (var i=0; i<completedTasksHolder.children.length;i++){
  //bind events to list items chldren(tasksIncompleted)
    bindTaskEvents(completedTasksHolder.children[i],taskIncomplete);
  }




// Issues with usabiliy don't get seen until they are in front of a human tester.

//prevent creation of empty tasks.

//Shange edit to save when you are in edit mode.