var global= /^[A-Za-z]+$/;
// First Name
$(document).ready(function()
{
  $("#fname").blur(function()
  {
    var name = $("#fname").val();
    // var name= $('#fname').val();
    if(name.length===0)
    {
       $("#fn").html("Enter Name");
       return false;
    }
     else
    {
      $('#fn').html(" ");
    }
    if(name.match(global))
    {
      return true;
    }
    else
    {
      $('#fn').html("Enter Characters Only");
      return false;
    }
  }); 
  
  // Last Name

  $("#lastname").blur(function()
  {
    var lname = $("#lastname").val();
    var name= $('#lastname').val();
    console.log(name)
    var b=Number(name.length);
    console.log(b);
    if(b==0)
    {
       $("#ln").html("Enter Name");
       return false;
    }
    else
    {
      $('#ln').html(" ");
    }
    if(lname.match(global))
    {
      true;
    }
    else
    {
      $('#ln').html("Enter Characters Only");
      return false;
    }
  });

  // Phone Number

  $("#phone").blur(function()
  {
    var num = $('#phone').val();
    var b=Number(num.length);
    console.log(b);
    if(b===0)
    {
      console.log("hello");
      $("#ph").html("<font color='red'>Mandatory Field</font>");
      return false;
    }
    if (isNaN(num))
    {  
      $("#ph").html("Enter Numeric value only");  
      return false;  
    }
    else
    {
      $("#ph").html("");
    } 
    if(num.startsWith("+")) {
      $("#phone").maxLength = 13;
      if (num.length==13) 
      {
        $("#ph").html(""); 
      }
      else
      {
        $("#ph").html("Enter 12 Number");
      }
        return true;
    }
    else 
    {
      $("#phone").maxLength = 10;
      if (num.length==10) 
      {
        $("#ph").html(""); 
      }
      else{
      $("#ph").html("Enter 10 Number");}
    }
  });

  // Office Number

  $("#office").blur(function()
  {
    var off = $("#office").val();
    var b=Number(off.length);
    if(b===0)
    {
      $("#num").html("Mandatory Field");
      return false;
    }
    if (isNaN(off))
    {  
      $("#num").html("Enter Numeric value only");  
      return false;  
    }
    else
    {
      $('#num').html(" ");  
    }
  }); 

  // Emali Valid

  $("#email").blur(function()
  {
    var a = $('#email').val();
    var pattern = /^\S\D\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/; 
    var pat=/^\D\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}?\.[a-zA-Z]{2}$/;
    var pa = /^\D\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/; 
    var b=Number(a.length);
    if(b===0)
    {
      $("#ml").html("Mandatory Field");
      return false;
    }
    else
    {
      $('#ml').html(""); 
    }
    if(a.match(pa) && a.match(pattern) || a.match(pat))
    {
      //return true;
      // document.getElementById('ml').innerHTML="Valid";
    }
    else
    {
      $('#ml').html("Enter correct mail id");
      return false;
    }
  });
  $("#password").blur(function()
  {
    var pass = $("#password").val();
    var c=/(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,12})$/;
    var letter = /[a-zA-Z]/; 
    var number = /[0-9]/;
    var b=Number(pass.length);
    if(b===0)
    {
      $("#pass").html("Mandatory Field");
      return false;
    }
    else
    {
      $('#pass').html(" "); 
    }
    if(pass.match(letter) && pass.match(number))
    {
        $('#pass').html("");
    }
    else
    {
      $('#pass').html("Enter Alphanumeric Value Only");
      return false;
    }  
    if(pass.length <= 12 && pass.length >= 8)
    {
      $('#pass').html("");
    }
    else
    {
      $('#pass').html("make sure the input is between 8-12 characters long");
    }
  }); 

// Confirm Password

  $("#cnpassword").blur(function()
  {
    var a = $('#password').val();
    var b = $('#cnpassword').val();
    var c=Number(a.length);
    if(c===0)
    {
      $("#cnpass").html("Mandatory Field");
      return false;
    }
    else
    {
      $('#cnpass').html(""); 
    } 
    if(a==b)
    {  
      // return true;  
    }  
    else
    {  
      $('#cnpass').html("Password does not match");
      return false;  
    }
  });

  // Age Calculation

  $("#day, #month, #year, #calc").change(function()
  {
    var date=new Date;
    var birthMonth=$('#month').val();
    
    var birthYear=$('#year').val();
    
    var birthDay=$('#day').val();
    // console.log(birthDay);
    // console.log(birthYear);
    // console.log(birthMonth);
    if((birthMonth==('Month'))||(birthYear==('Year'))||(birthDay==('Day')))
    {
      
      $("dobMsg").html("Select DOB");
      return false;
    }
    else
    {
      var dob=new Date(birthYear,birthMonth/10,birthDay);
      // console.log(birthMonth);
      var m = date.getMonth()+1;
      console.log(m);
      var y = date.getFullYear()-1;
      console.log(y);
      console.log("birthMonth");
      console.log(birthMonth);
      var q = birthMonth-m;
      console.log(q);
      var age = (y-birthYear)+(Math.abs(12-q)/10)
      console.log(age);
      $('#calc').val(age);
      $('calc').disabled = true;
    } 
  });

  // Check Box

  $("#checkbox_sample18, #checkbox_sample19, #checkbox_sample20").change(function()
  {
    if($("#checkbox_sample18").is(":checked") || $("#checkbox_sample18").is(":checked") || $("#checkbox_sample18").is(":checked"))
    {
      $('#cx').html("");
    } 
    else {
      $('#cx').html("Select At least one box");

    }
  });
  
  // About You

  $("#about").blur(function()
  {
    var detail=$("#about").val();
    var detaillen=Number(detail.length);
    if(detaillen===0)
    {
      $('#au').html("Mandatory Field");
      return false;
    }
    else
    {
      $('#au').html(""); 
    }
  });
});
